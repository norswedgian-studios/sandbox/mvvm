//
//  Logger.swift
//  MVVM
//
//  Created by Tim Fuqua on 11/21/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

/// FFL - File Function Line
private struct FFL: CustomStringConvertible {
    let file: String?
    let function: String?
    let line: String?
    
    var description: String {
        var result = [file, function].compactMap({ $0 }).filter({ $0.isEmpty == false }).joined(separator: " *** ")
        
        if let line = line, line != "0" {
            result += "[\(line)]"
        }
        
        return result
    }
}

/// Example output:
/// Swift.print("\u{1F534} \(file) *** \(function)[\(line)]:\n\t\(bracketedPrefix)\(message)")
///
/// - Parameters:
///   - marker: (optional) The marker is placed left aligned in the logger at the beginning of the message; defaults to empty
///   - file: (optional) The file is the filename, left aligned to the marker (if it exists); defaults to #file, and strips out the path
///   - function: (optional) The function name, left aligned to the file (if it exists) and separated by " *** "; defaults to #function
///   - line: (optional) The line number, left aligned to the function (if it exists) and wrapped with []; defaults to #line
///   - prefix: (optional) The prefix value, on a new line and tabbed in once and left aligned, will be wrapped in [] and immediately preceeds the message; defaults to empty
///   - message: (optional) The message, left aligned to the prefix value (if it exists); defaults to empty
/// - Returns: The fully formatted string that was printed to the console
@discardableResult
func prefixPrint(
    marker: String? = nil,
    file: String? = #file,
    function: String? = #function,
    line: Int? = #line,
    prefix: String? = nil,
    message: String? = nil
) -> String {
    let logTime: Bool = false
    let file = file?.components(separatedBy: "/").last ?? "Unknown file"
    let function = function ?? ""
    let line = String(optional: line) ?? ""
    let ffl = FFL(file: file, function: function, line: line)
    var result = logTime ? "\(Date().iso8601) " : ""
    
    if let marker = marker {
        result += marker
    }
    
    if ffl.description.isEmpty == false {
        result += ffl.description
    }
    
    if result.isEmpty == false {
        var bracketedPrefix = ""
        
        if let prefix = prefix, prefix.isEmpty == false {
            bracketedPrefix = "[\(prefix)]"
        }
        
        let message = message ?? ""
        
        if bracketedPrefix.isEmpty == false || message.isEmpty == false {
            result += ":\n\t"
            
            result += [bracketedPrefix, message].joined(separator: " ")
        }
    }
    
    print(result)
    return result
}
