//
//  DateConverterModel.swift
//  MVVM
//
//  Created by Tim Fuqua on 11/21/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

class DateConverterModel: NSObject, Codable {
    @objc dynamic var epoch: TimeInterval
    
    init(epoch: TimeInterval) {
        self.epoch = epoch
    }
}
