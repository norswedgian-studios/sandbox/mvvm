//
//  DateConverterViewModel.swift
//  MVVM
//
//  Created by Tim Fuqua on 11/21/20.
//

import Foundation

protocol DateConverterViewModelInputs: AnyObject {
    func didAppear()
    func refreshButtonPressed()
    func epochRawInputChanged(to epoch: Int)
}

protocol DateConverterViewModelOutputs: AnyObject {
    // MARK: Model
    var dateText: String { get }
    var iso8601Text: String { get }
    var epochRaw: TimeInterval { get }
    var epochTruncated: Int { get }
}

protocol DateConverterViewModelType: AnyObject {
    var inputs: DateConverterViewModelInputs { get }
    var outputs: DateConverterViewModelOutputs { get }
}

class DateConverterViewModel: NSObject, DateConverterViewModelType, DateConverterViewModelInputs, DateConverterViewModelOutputs {
    // MARK: DateConverterViewModelType vars
    var inputs: DateConverterViewModelInputs { self }
    var outputs: DateConverterViewModelOutputs { self }
    
    // MARK: DateConverterViewModelOutputs vars
    @objc dynamic var dateText: String = ""
    @objc dynamic var iso8601Text: String = ""
    @objc dynamic var epochRaw: TimeInterval = 0
    @objc dynamic var epochTruncated: Int = 0
    
    // MARK: Injected vars
    private var model: DateConverterModel
    
    // MARK: private vars
    private var observations: [Observation] = []
    private var dateFromEpoch: Date { Date(timeIntervalSince1970: epochRaw) }
    
    init(model: DateConverterModel) {
        self.model = model
        super.init()
        postInit()
    }
    
    override init() {
        // Use Repository here
        model = DateConverterModel(epoch: Date().timeIntervalSince1970)
        super.init()
        postInit()
    }
    
    private func postInit() {
        // Observe the model
        observations += [model.observe(\.epoch) { epoch in
            self.epochRaw = epoch
            self.dateText = self.dateFromEpoch.localTimeShort
            self.iso8601Text = self.dateFromEpoch.iso8601
            self.epochTruncated = Int(self.epochRaw)
        }]
    }
}

// MARK:- DateConverterViewModelInputs
extension DateConverterViewModel {
    func didAppear() {
        prefixPrint()
    }
    
    func refreshButtonPressed() {
        prefixPrint()
        model.epoch = Date().timeIntervalSince1970
    }
    
    func epochRawInputChanged(to epoch: Int) {
        prefixPrint()
        model.epoch = TimeInterval(epoch)
    }
}
