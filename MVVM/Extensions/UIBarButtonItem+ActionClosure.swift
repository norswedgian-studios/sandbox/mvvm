//
//  UIBarButtonItem+ActionClosure.swift
//  MVVM
//
//  Created by Tim Fuqua on 11/21/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import UIKit

typealias VoidBlock = (() -> Void)

// adapted from https://stackoverflow.com/a/44037534
extension UIBarButtonItem {
    private struct AssociatedObject {
        static var key = "action_closure_key"
    }
    
    var actionClosure: VoidBlock? {
        get {
            return objc_getAssociatedObject(self, &AssociatedObject.key) as? VoidBlock
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObject.key, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            target = self
            action = #selector(didTapButton(sender:))
        }
    }
    
    @objc func didTapButton(sender: Any) {
        actionClosure?()
    }
    
    convenience init(title: String?, style: UIBarButtonItem.Style, target: Any?, action: @escaping VoidBlock) {
        self.init(title: title, style: style, target: target, action: nil)
        self.actionClosure = action
    }
}
