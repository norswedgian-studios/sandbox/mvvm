//
//  UITextField+Toolbar.swift
//  MVVM
//
//  Created by Tim Fuqua on 11/21/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import UIKit

extension UITextField {
    func addToolbarToKeyboard(leftButton left: UIBarButtonItem?, rightButton right: UIBarButtonItem?, toolbarHeight height: CGFloat = 50.0) {
        guard left != nil || right != nil else { return }
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height))
        doneToolbar.translatesAutoresizingMaskIntoConstraints = false
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let items = [left, flexSpace, right].compactMap { $0 }
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        inputAccessoryView = doneToolbar
    }
}
