//
//  String.swift
//  MVVM
//
//  Created by Tim Fuqua on 11/21/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

extension String {
    init?<T: LosslessStringConvertible>(optional: T?) {
        guard let optional = optional else { return nil }
        self = String(optional)
    }
}
