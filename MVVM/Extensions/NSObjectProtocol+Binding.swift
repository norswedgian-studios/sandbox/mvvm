//
//  NSObjectProtocol+Binding.swift
//  MVVM
//
//  Created by Tim Fuqua on 11/21/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// MARK: - Bindings with KVO and Keypaths
// Adapted from https://www.objc.io/blog/2018/04/24/bindings-with-kvo-and-keypaths/
extension NSObjectProtocol where Self: NSObject {
    typealias Observation = NSKeyValueObservation
    
    func observe<Value>(
        _ keyPath: KeyPath<Self, Value>,
        onChange: @escaping (Value) -> Void
    ) -> Observation {
        return observe(keyPath, options: [.initial, .new]) { _, change in
            // TODO: change.newValue should never be `nil`, but when observing an optional property
            // that's set to `nil`, then change.newValue is `nil` instead of `Optional(nil)`. This
            // is the bug report for this: https://bugs.swift.org/browse/SR-6066
            guard let newValue = change.newValue else { return }
            onChange(newValue)
        }
    }
    
    func bind<Value, Target>(
        _ sourceKeyPath: KeyPath<Self, Value>,
        to target: Target,
        at targetKeyPath: ReferenceWritableKeyPath<Target, Value?>
    ) -> Observation {
        return observe(sourceKeyPath) { target[keyPath: targetKeyPath] = $0 }
    }
}
