//
//  Date+HumanReadable.swift
//  MVVM
//
//  Created by Tim Fuqua on 11/21/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

// Hours short string (ie: "12pm")
extension Date {
    /// Formatter that outputs as just the hour number and "am"/"pm"
    ///  ie: "12am", "1am", ..., "12pm", "1pm"
    private static let hoursShortFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "ha"
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"
        return formatter
    }()
    
    /// "12am", "1am", ..., "12pm", "1pm"
    var hourShortString: String { Date.hoursShortFormatter.string(from: self) }
}

// Local time string (ie: "01:23:45.678")
extension Date {
    private static let localTimeMinimalDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss.SSS"
        formatter.timeZone = TimeZone.current
        return formatter
    }()
    
    var localTimeMinimal: String { Date.localTimeMinimalDateFormatter.string(from: self) }
}

// Local time string (ie: "01:23:45.678")
extension Date {
    private static let localTimeShortDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        formatter.timeZone = TimeZone.current
        return formatter
    }()

    var localTimeShort: String { Date.localTimeShortDateFormatter.string(from: self) }
}
