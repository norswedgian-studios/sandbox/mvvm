//
//  Date+ISO8601.swift
//  MVVM
//
//  Created by Tim Fuqua on 11/21/20.
//  Copyright © 2020 Norswedgian Studios. All rights reserved.
//

import Foundation

extension Date {
    /// Formatter that outputs as ISO8601 formatting
    ///  ie: 1970-01-01T00:00:00.000Z
    static let iso8601Formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
    
    /// ie: 1970-01-01T00:00:00.000Z
    var iso8601: String { Date.iso8601Formatter.string(from: self) }
}
