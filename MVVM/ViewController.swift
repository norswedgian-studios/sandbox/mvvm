//
//  ViewController.swift
//  MVVM
//
//  Created by Tim Fuqua on 11/21/20.
//

import UIKit

class ViewController: UIViewController {

    // MARK: injected vars
    private var viewModel: DateConverterViewModel!
    
    // MARK: private vars
    private var keyboardOffset: CGFloat = 0
    private var observations: [Observation] = []

    // MARK: @IBOutlets
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var iso8601Label: UILabel!
    @IBOutlet weak var epochInputTextField: UITextField!
    @IBOutlet weak var copyButton: UIButton!
    @IBOutlet weak var refreshButton: UIButton!
    
}

// MARK: life cycle
extension ViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        prefixPrint()
        
        epochInputTextField.delegate = self
        epochInputTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        epochInputTextField.addToolbarToKeyboard(
            leftButton: UIBarButtonItem(title: "Dismiss", style: .plain, target: self) {
                _ = self.epochInputTextField.resignFirstResponder()
            }
            , rightButton: nil
        )
        
        datePicker.addTarget(self, action: #selector(datePickerDidChange(_:)), for: .valueChanged)
        timePicker.addTarget(self, action: #selector(datePickerDidChange(_:)), for: .valueChanged)

        // Normally, this would be injected into VC by Router
        viewModel = DateConverterViewModel()
        
        // MARK: MVVM Binding
        observations += [viewModel.bind(\.dateText, to: dateLabel, at: \.text)]
        observations += [viewModel.bind(\.iso8601Text, to: iso8601Label, at: \.text)]
        observations += [viewModel.observe(\.epochTruncated, onChange: { epochTruncated in
            self.epochInputTextField.text = "\(epochTruncated)"
        })]
        observations += [viewModel.observe(\.epochRaw, onChange: { epochRaw in
            self.datePicker.setDate(Date(timeIntervalSince1970: TimeInterval(epochRaw)), animated: true)
            self.timePicker.setDate(Date(timeIntervalSince1970: TimeInterval(epochRaw)), animated: true)
        })]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        prefixPrint()
    }
}

// MARK: @IBActions
extension ViewController {
    @IBAction func refreshButtonTapped(_ sender: UIButton) {
        prefixPrint()
        
        viewModel.refreshButtonPressed()
    }
    
    @IBAction func copyButtonTapped(_ sender: UIButton) {
        prefixPrint()
        
        UIPasteboard.general.string = "\(viewModel.epochTruncated)"
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        prefixPrint(message: "\(sender.text ?? "")")
        
        guard let senderText = sender.text, let epoch = Int(senderText) else { return }
        viewModel.epochRawInputChanged(to: epoch)
    }
    
    @objc private func datePickerDidChange(_ sender: UIDatePicker) {
        prefixPrint(message: "\(sender.date)")
        
        viewModel.epochRawInputChanged(to: Int(sender.date.timeIntervalSince1970))
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        keyboardOffset = (UIScreen.main.bounds.height - (textField.frame.origin.y + textField.frame.height))
//        view.frame = view.frame.offsetBy(dx: 0, dy: -keyboardOffset)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        view.frame = view.frame.offsetBy(dx: 0, dy: keyboardOffset)
    }
}
